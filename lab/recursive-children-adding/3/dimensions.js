// COLORS

var grey = [
    '#000000',
    '#111111',
    '#222222',
    '#333333',
    '#444444',
    '#555555',
    '#666666',
    '#777777',
    '#888888',
    '#999999',
    '#AAAAAA',
    '#BBBBBB',
    '#CCCCCC',
    '#DDDDDD',
    '#EEEEEE', 
    '#FFFFFF'
]

var cssColors = [
    'tan',
    'peru',
    'fuchsia',
    'aquamarine',
    'cadetblue',
    'violet',
    'indigo',
    'sienna',
    'olive',
    'steelblue',
    'maroon',
    'slategrey',
    'indianred',
    'tomato',
    'mediumblue',
    'royalblue',
    'sandybrown',
    'skyblue',
    'rebeccapurple',
    'springgreen',
    'khaki',
    'grey',
    'firebrick',
    'deeppink',
    'salmon',
    'blue',
    'dimgray',
    'mediumaquamarine',
    'blueviolet',
    'thistle',
    'chartreuse',
    'teal',
    'coral',
    'plum',
    'forestgreen',
    'lawngreen',
    'navy',
    'lime',
    'purple',
    'hotpink',
    'midnightblue',
    'pink',
    'mediumseagreen',
    'slategray',
    'burlywood',
    'olivedrab',
    'greenyellow',
    'mediumslateblue',
    'magenta',
    'mediumturquoise',
    'snow',
    'silver',
    'azure',
    'cornflowerblue',
    'ivory',
    'wheat',
    'goldenrod',
    'chocolate',
    'slateblue',
    'limegreen',
    'red',
    'moccasin',
    'yellowgreen',
    'green',
    'mediumpurple',
    'orange',
    'deepskyblue',
    'saddlebrown',
    'dimgrey',
    'gold',
    'mediumspringgreen',
    'rosybrown',
    'crimson',
    'orchid',
    'peachpuff',
    'mistyrose',
    'powderblue',
    'brown',
    'cyan',
    'dodgerblue',
    'turquoise',
    'mediumorchid',
    'seagreen',
    'mediumvioletred',
    'gray',
    'aqua',
    'orangere',
    'black'
];

// UTILS

function rdm(min, max) {
	return {
		float:Math.random() * (max - min) + min,
		int:parseInt(Math.random() * (max - min) + min)
	}
}

function rdmArr(arr) {
        var n = rdm(0,arr.length).int;
        return arr[n];
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}


// OBJECTS

Dimension = function(options) {
    base = {};
    for (var k in base) {
      this[k] = base[k];
    }
    this.update(options);
}

Dimension.prototype.update = function(options) {
    for (var k in options) {
      this[k] = options[k];
    }
}

// FUNCTIONS

function add(context, numberofchildren, maxlevel) {
    if (context.level < maxlevel) {
        for (var i = 0; i < numberofchildren; i ++) {
            options = {}
            options.level = context.level+1;
            options.color = rdmArr(cssColors);
            options.orientation = rdmArr(orientation);
            options.text = rdmArr(words);
            options.children = [];
            child = new Dimension(options);
            context.children.push(child);
            add(child, rdm(0,5).int, maxlevel)
            //add(child, 2, maxlevel)
        }
    }
}

async function make(container, object) {
    var div = document.createElement('div');
    //div.style.background = object['color'];
    div.style.flexDirection = object['orientation'];
    div.innerText = object['text'] + " ";
    //var p = document.createElement('p');
    //p.innerText = object['text'];
    //div.append(p);
    container.append(div);
    for (var k in object['children']) {
        await sleep(200);
        //console.log(object['children'][k]);
        make(div, object['children'][k]);
    }
}

// RUN

var colors = ['black', 'green', 'blue']
var orientation = ['row','column']
var par = 'In physics and mathematics, the dimension of a mathematical space (or object) is informally defined as the minimum number of coordinates needed to specify any point within it. Thus a line has a dimension of one because only one coordinate is needed to specify a point on it – for example, the point at 5 on a number line. A surface such as a plane or the surface of a cylinder or sphere has a dimension of two because two coordinates are needed to specify a point on it – for example, both a latitude and longitude are required to locate a point on the surface of a sphere. The inside of a cube, a cylinder or a sphere is three-dimensional because three coordinates are needed to locate a point within these spaces.';
console.log(par);
var words = par.split(" ");
console.log(words);


options = {}
options.level = 0;
options.color = rdmArr(cssColors);
options.orientation = rdmArr(orientation);
options.children = [];
options.text = rdmArr(words);
root = new Dimension(options);


//add(root, rdm(1,8).int, 4)
//add(root, rdm(1,5).int, 4)
add(root, rdm(1,5).int, 10)
//add(root, 2, 4)



console.log(root);
make(document.body, root);
