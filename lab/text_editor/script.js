var matrix = new Dimension(settings.dimensions.new.domMatrix);
var sizeTest=document.createElement('span');
sizeTest.textContent=' ';
sizeTest.className='white';
document.body.appendChild(sizeTest);
var nTypes=parseInt((window.innerWidth/sizeTest.getBoundingClientRect().width));
var nLines=parseInt((window.innerHeight/sizeTest.getBoundingClientRect().height));
sizeTest.style.display='none';
matrix.add([
	['line',
	{
		datas:{line:true},
	},nLines],
	['type',
	{
		datas:{type:true},
	},nTypes]
	]);
/*
document.body.appendChild(matrix.display.dom);
var types=document.getElementsByTagName('td');
for(var i in types)types[i].textContent=' ';
matrix.display.dom.style.display='none';
*/
////////////////////////////////////////
function fallbackCopyTextToClipboard(text) {
	var textArea = document.createElement("textarea");
	textArea.value = text;
	document.body.appendChild(textArea);
	textArea.focus();
	textArea.select();

	try {
		var successful = document.execCommand('copy');
		var msg = successful ? 'successful' : 'unsuccessful';
		console.log('Fallback: Copying text command was ' + msg);
	} catch (err) {
		console.error('Fallback: Oops, unable to copy', err);
	}

	document.body.removeChild(textArea);
}
function copyTextToClipboard(text) {
	if (!navigator.clipboard) {
		fallbackCopyTextToClipboard(text);
		return;
	}
	navigator.clipboard.writeText(text).then(function() {
		console.log('Async: Copying to clipboard was successful!');
	}, function(err) {
		console.error('Async: Could not copy text: ', err);
	});
}

function getText(){
	return matrix.display['string'].join('\r')
}

document.onmousedown=function(){
	//copyTextToClipboard(getText());
}
console.log(matrix);

// Ici, la requête sera émise de façon synchrone.
function request(){
	const req = new XMLHttpRequest();
	req.open('GET', 'script.php', false); 
	req.send(null);

	if (req.status === 200) {
		console.log("Réponse reçue: %s", req.responseText);
	} else {
		console.log("Status de la réponse: %d (%s)", req.status, req.statusText);
	}
}

function write(content,file,mode) {
	if(!mode)mode='a';
	writeRequest={
		file:file,
		content:content,
		mode:mode
	}
	request= new XMLHttpRequest()
	request.open("POST", "script.php", true)
	request.setRequestHeader("Content-type", "application/json")
	request.send(JSON.stringify(writeRequest))
	request.onreadystatechange = function () {
		if (request.readyState == 4 && request.status == "200") {
			console.log(request.responseText);
		}
	}
}
function read(url) {   
	var xobj = new XMLHttpRequest();
	xobj.open('GET', url, true);
	xobj.send(null);
	var promise = new Promise(function(resolve, reject) {
		xobj.onreadystatechange = function () {
			if (xobj.readyState == 4 && xobj.status == "200") {
				resolve(xobj.responseText);
			}
		}
	});
	return promise
}
var updated;
function update(){
	read('text').then(function(content) {
		//console.log(content,document.body.innerText);
		if(content!=updated){
			document.body.innerText=content;
			updated=content
		}
		setTimeout(function(){
			update();
		},17);
	});
}

update();

document.onkeydown=function(e){
	if(e.key.length==1){
		write(e.key,'text','a');
	}
	if(e.key=='Enter'){
		write('\n','text','a');
	}
}