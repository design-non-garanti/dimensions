Dimension = function(options){
	local_settings=JSON.parse(JSON.stringify(settings.dimensions.new.default))
	options=JSON.parse(JSON.stringify(options))
	for(var s in local_settings)this[s]=local_settings[s]
	for(var o in options)this[o]=options[o]
	if(!this.display.length)this.createDisplay()
}

Dimension.prototype.add = function(options,depth,context) {
	if(!context)context=[];
	if(!depth)depth=0;
	for(var i = 0 ; i<options[depth][2] ; i++){
		if(!this.content[options[depth][0]])this.content[options[depth][0]]=[]
		var dim = new Dimension(options[depth][1])
		dim.position=i;
		dim.context=[this].concat(context)
		if(depth+1<options.length)dim.add(options,depth+1,this)
		dim.update();	
		if(!dim.display.length && !dim.context[0].display.length)dim.buildDisplay()
		this.content[options[depth][0]].push(dim)
	}
}
Dimension.prototype.update = function(){
	if(this.context[0])if(!this.context[0].display['string'])this.context[0].display['string']=[];
	var className=''
	this.display.char=' ';
	for(var i in this.datas){
		if(this.datas[i]){
			if(i.length==1){
				this.display.char=i;
				if(i==' ')this.display.char=' ';
			}else{
				className+=i+' ';
			}
		}
	}
	if(this.datas['INT'])this.display.char=this.datas['INT'];
	if(this.datas['█'])this.display.char='█';
	if(this.context[0])if(this.context[0].display['string']){
		//console.log(this.display)
		this.context[0].display['string'][this.position]=this.display['char'];
				this.context[0].update();
	}
	if(this.context[0])if(this.context[0].display['string'] && this.display['string'])this.context[0].display['string'][this.position]=this.display['string'].join("");
	this.display.class=className;
	return className
}

Dimension.prototype.findRelay = function(dir,dim,position){
	if(!dim)dim=0;
	if(dim>dir.length-1)return position
		var parent = this.context[this.context.length-dim-1][this.context[this.context.length-dim-2].class][dir[dir.length-dim-1]+this.context[this.context.length-dim-2].position];
	if(parent){
		if(!position){
			position=parent;
		}else{
			position=position[this.context[this.context.length-dim-2].class][dir[dir.length-dim-1]+this.context[this.context.length-dim-2].position];
		}
	}else{
		return false
	}
	dim++;
	return this.findRelay(dir,dim,position)
}

Dimension.prototype.createDisplay = function(){
	if(this.display['dom'])this.display.dom=document.createElement(this.display.dom)
}
Dimension.prototype.buildDisplay = function(){
	if(this.display['dom'] && this.context[0].display['dom'])this.context[0].display.dom.appendChild(this.display.dom)
}

