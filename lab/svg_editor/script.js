var matrix = new Dimension(settings.dimensions.new.domMatrix);
matrix.add([
	['line',
	{
		datas:{line:true},
		display:{
			dom:'tr',
		}
	},32],
	['type',
	{
		datas:{type:true},
		display:{
			dom:'td',
		}
	},64]
]);
document.body.appendChild(matrix.display.dom);
var types=document.getElementsByTagName('td');
for(var i in types)types[i].textContent=' ';	