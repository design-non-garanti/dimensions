#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json

# SETTING
'''
with open('data.json') as json_data:
    data = json.load(json_data)
'''
# CLASSES

class Dimension:

    def __init__(self,options):
        with open('settings.json') as json_settings:
            settings = json.load(json_settings)
        for i in settings['dimension']:
            setattr(self, i, settings['dimension'][i])
        for i in options:
            setattr(self, i, options[i])

    def add(self,*options):
        index=0
        if len(options) == 1:
            depth=0
        else:
            depth=options[1]
        for dimensions in options[0]:
            if index == depth :
                if(dimensions in self.content)==False:
                    self.content[dimensions]=[]
                for i in range(0,options[0][dimensions]):
                    dimension = Dimension({'id':dimensions+str(i)})
                    self.content[dimensions].append(dimension)
                    dimension.add(options[0],index+1)
            index+=1

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)

root = Dimension({'id':'root'})

root.add({'layer':16,'line':16,'type':16})

with open('data.json', 'w') as outfile:
    json.dump(json.loads(root.toJSON()), outfile)

