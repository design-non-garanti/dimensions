/* 
 * js cheats 
 */

function write(content,file,mode) {
	if(!mode)mode='w'
	writeRequest={
		file:file,
		content:content,
		mode:mode
	}
	request= new XMLHttpRequest()
	request.open("POST", "script.php", true)
	request.setRequestHeader("Content-type", "application/json")
	request.send(JSON.stringify(writeRequest))
}

function read(url) {   
	xobj = new XMLHttpRequest()
	xobj.open('GET', url, true)
	xobj.send(null)
	promise = new Promise(function(resolve, reject) {
		xobj.onreadystatechange = function () {
			if (xobj.readyState == 4 && xobj.status == "200") {
				resolve(xobj.responseText)
			}
		}
	})
	return promise
}

/* 
 * Dimension
 */

var dimensions=[]

Dimension = function(options){
	var setting=JSON.parse(JSON.stringify(settings.dimension));
	for(var i in setting)this[i]=setting[i]
	for(var i in options)this[i]=options[i]
	this.id=dimensions.length
	dimensions.push(this)
	//for(var i in options)this[i]=options[i]
}

Dimension.prototype.add = function(options,depth) {
	var index=0
	if(!depth)depth=0
	for(var dimensions in options){
		if(index==depth){
			depth++
			for(var i = 0 ; i < options[dimensions]; i++){
				var dimension = new Dimension()
				dimension.type=dimensions
				dimension.position=i
				dimension.context=this
				dimension.add(options,depth)
				this.content.push(dimension)
			}
			break
		}
		index++
	}
}

Dimension.prototype.findRelative = function(dir) {
	path=[this]
	positions=[]
	for(var i in dir){
		path.push(path[path.length-1].context)
		if(i==dir.length-1){
			relative=path[path.length-1]
			for(var j = dir.length-1 ; j > -1 ; j--){
				if(!relative)return
				relative=relative.content[path[j].position+dir[j]]
			}
			return relative
		}
	}
}

Dimension.prototype.save = function(datas){
	if(!datas)datas=[]
	data={}
	content=[]
	for(var i in this.content)content.push(this.content[i].id)
	for(var i in this){
		if(i!= 'id' && typeof this[i] != "function" && !(this[i] instanceof HTMLElement))data[i]=this[i]
	}
	data.content=content
	if(this.context!=undefined){
		data.context=this.context.id
	}else{
		data.context=false
	}
	
	for(var i in data.datas)if(!isNaN(parseFloat(i)))delete data.datas[i]
	datas.push(data)
	for(var i in this.content)this.content[i].save(datas)
	return datas
}


/* 
 * setup
 */


read('settings.json').then(function(content) {
	settings=JSON.parse(content)
	setup()
})

